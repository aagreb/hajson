{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Lexer where

import qualified Data.Text.Lazy as T
import qualified Data.Char as C
import qualified Numeric as N

data Token = TTrue | TFalse | TNull | TObjectOpen | TObjectClose | TComma | TColon | TArrayOpen | TArrayClose | TString T.Text | TNumber Double| TSentinel deriving Show

data TokenizeCondition = Neutral | Identifier (Token, T.Text) | NumStart | NumDigits | LeadingMinusSign | FractionPoint | FractionDigits | ExponentSign | ExponentDigits | String | StringEscape | StringEscapeHex

data TokenizeState = TokenizeState
    { identifierAcc :: T.Text
    , stringAcc :: T.Text
    , hexEscapeAcc :: Int
    , numAcc :: T.Text
    }

neutralTokenizeState :: TokenizeState
neutralTokenizeState = TokenizeState
    { identifierAcc = ""
    , stringAcc = ""
    , hexEscapeAcc = 0
    , numAcc = ""
    }

tokenize :: T.Text -> Maybe [Token]
tokenize inp = (++ [TSentinel]) <$> tokenize' neutralTokenizeState Neutral (T.snoc inp '\0')

tokenize' :: TokenizeState -> TokenizeCondition -> T.Text -> Maybe [Token]
tokenize' _ _ "" = Just [TSentinel]
tokenize' state condition input = do
    (char, remainder) <- T.uncons input
    let neutralRemainder token = (token :) <$> tokenize' state Neutral remainder
        numberFromState =  case N.readSigned N.readFloat $ T.unpack state.numAcc of
            [(num, _)] -> num
            _ -> 0
    case condition of
        Neutral -> case char of
            '{' -> neutralRemainder TObjectOpen
            '}' -> neutralRemainder TObjectClose
            '[' -> neutralRemainder TArrayOpen
            ']' -> neutralRemainder TArrayClose
            ',' -> neutralRemainder TComma
            ':' -> neutralRemainder TColon
            't' -> tokenize' state (Identifier (TTrue, "true")) input
            'f' -> tokenize' state (Identifier (TFalse, "false")) input
            'n' -> tokenize' state (Identifier (TNull, "null")) input
            '"' -> tokenize' state String remainder
            '-' -> tokenize' state LeadingMinusSign input
            c | C.isDigit c -> tokenize' state NumStart input
            c | c `elem` [' ', '\n', '\r', '\t'] -> tokenize' state Neutral remainder
            '\0' -> Just []
            _ -> Nothing
        Identifier (token, name) -> case T.snoc state.identifierAcc char of
            acc | acc == name -> (token :) <$> tokenize' state {identifierAcc = ""} Neutral remainder
            acc | T.isPrefixOf acc name -> tokenize' state {identifierAcc = acc} (Identifier (token, name)) remainder
            _ -> Nothing
        String -> case char of
            '\\' -> tokenize' state StringEscape remainder
            '"' -> (TString state.stringAcc :) <$> tokenize' state {stringAcc = ""} Neutral remainder
            c | C.ord c >= 0x20 && C.ord c <= 0x10FFF -> tokenize' state {stringAcc = T.snoc state.stringAcc c} String remainder
            _ -> Nothing
        StringEscape -> case char of
            'u' -> tokenize' state StringEscapeHex remainder
            '"' -> tokenize' state {stringAcc = T.snoc state.stringAcc char} String remainder
            '\\' -> tokenize' state {stringAcc = T.snoc state.stringAcc char} String remainder
            '/' -> tokenize' state {stringAcc = T.snoc state.stringAcc char} String remainder
            'b' -> tokenize' state {stringAcc = T.snoc state.stringAcc '\b'} String remainder
            'f' -> tokenize' state {stringAcc = T.snoc state.stringAcc '\f'} String remainder
            'n' -> tokenize' state {stringAcc = T.snoc state.stringAcc '\n'} String remainder
            'r' -> tokenize' state {stringAcc = T.snoc state.stringAcc '\r'} String remainder
            't' -> tokenize' state {stringAcc = T.snoc state.stringAcc '\t'} String remainder
            _ -> Nothing
        StringEscapeHex ->
            case N.readHex [char] of
                [(numericValue, _)] -> tokenize' state {hexEscapeAcc = state.hexEscapeAcc * 16 + numericValue} StringEscapeHex remainder
                _ -> tokenize' state {stringAcc = T.snoc state.stringAcc (C.chr state.hexEscapeAcc)} String input
        LeadingMinusSign ->
            case char of
                '-' -> tokenize' state {numAcc = T.snoc state.numAcc char} NumStart remainder
                _ -> Nothing
        NumStart -> case char of
            '0' -> tokenize' state {numAcc = T.snoc state.numAcc char} FractionPoint remainder
            c | C.isDigit c -> tokenize' state NumDigits input
            _ -> tokenize' state FractionPoint input
        NumDigits -> case char of
            c | C.isDigit c -> tokenize' state {numAcc = T.snoc state.numAcc char} NumDigits remainder
            _ -> tokenize' state FractionPoint input
        FractionPoint ->
            case C.toLower char of
                '.' -> tokenize' state {numAcc = T.snoc state.numAcc char} FractionDigits remainder
                'e' -> tokenize' state ExponentSign remainder
                '\0' -> Just [TNumber numberFromState]
                _ -> (TNumber numberFromState :) <$> tokenize' neutralTokenizeState Neutral input
        FractionDigits ->
            case C.toLower char of
                'e' -> tokenize' state {numAcc = T.snoc state.numAcc char} ExponentSign remainder
                '\0' -> Just [TNumber numberFromState]
                c | C.isDigit c -> tokenize' state {numAcc = T.snoc state.numAcc char} FractionDigits remainder
                _ -> (TNumber numberFromState :) <$> tokenize' neutralTokenizeState Neutral input
        ExponentSign ->
            case C.toLower char of
                '+' -> tokenize' state {numAcc = T.snoc state.numAcc char} ExponentDigits remainder
                '-' -> tokenize' state {numAcc = T.snoc state.numAcc char} ExponentDigits remainder
                _ -> tokenize' state ExponentDigits input
        ExponentDigits ->
            case char of
                '\0' -> Just [TNumber numberFromState]
                c | C.isDigit c -> tokenize' state {numAcc = T.snoc state.numAcc char} ExponentDigits remainder
                _ -> (TNumber numberFromState :) <$> tokenize' neutralTokenizeState Neutral input