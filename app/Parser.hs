{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DataKinds #-}

module Parser where
import qualified Data.Text.Lazy as T
import Lexer (Token (..))
import qualified Data.HashMap.Lazy as HM

data JsonValue = JsonObject (HM.HashMap T.Text JsonValue) | JsonArray [JsonValue] | JsonString T.Text | JsonNumber Double | JsonBoolean Bool | JsonNull deriving Show

type Member = (T.Text, JsonValue)

parse :: [Token] -> Maybe JsonValue
parse tokens = do
    (newTs, val) <- value tokens
    case newTs of
        [TSentinel] -> pure val
        _ -> Nothing

value :: [Token] -> Maybe ([Token], JsonValue)
value [] = Nothing
value (token : tokens) =
    let
        withValue = Just . (tokens,)
        ts = token : tokens
    in
    case token of
        TString str -> withValue $ JsonString str
        TNumber num -> withValue $ JsonNumber num
        TTrue       -> withValue $ JsonBoolean True
        TFalse      -> withValue $ JsonBoolean False
        TNull       -> withValue JsonNull
        TObjectOpen -> object ts
        TArrayOpen  -> array ts
        _ -> Nothing

object :: [Token] -> Maybe ([Token], JsonValue)
object (TObjectOpen : tokens) = objectTail tokens
object _ = Nothing

objectTail :: [Token] -> Maybe ([Token], JsonValue)
objectTail tokens =
    case tokens of
        (TString _ : _) -> do
            (TObjectClose : ts, membs) <- members tokens
            pure (ts, JsonObject $ HM.fromList membs)     
        (TObjectClose : ts) -> Just (ts, JsonObject HM.empty)
        _ -> Nothing

members :: [Token] -> Maybe ([Token], [Member])
members tokens =
    case tokens of
        (TString _ : _) -> do
            (memberTs, mem) <- member tokens
            (membersTs, mems) <- memberTail memberTs
            pure (membersTs, mem : mems)
        _ -> Nothing

memberTail :: [Token] -> Maybe ([Token], [Member])
memberTail tokens =
    case tokens of
        (TObjectClose : _) -> Just (tokens, [])
        (TComma : ts) -> members ts
        _ -> Nothing

member :: [Token] -> Maybe ([Token], Member)
member (TString str : TColon : ts) = do
    (valueTs, val) <- value ts
    pure (valueTs, (str, val))
member _ = Nothing

array :: [Token] -> Maybe ([Token], JsonValue)
array (TArrayOpen : ts) = do
    (tailTs, values) <- arrayTail ts
    pure (tailTs, JsonArray values)
array _ = Nothing

arrayTail :: [Token] -> Maybe ([Token], [JsonValue])
arrayTail tokens = do
    (elementTs, els) <- case tokens of
        (TString _ : _) -> elements tokens
        (TNumber _ : _) -> elements tokens
        (TTrue : _) -> elements tokens
        (TFalse : _) -> elements tokens
        (TNull : _) -> elements tokens
        (TObjectOpen : _) -> elements tokens
        (TArrayOpen : _) -> elements tokens
        (TArrayClose : _) -> Just (tokens, [])
        _ -> Nothing
    case elementTs of
        (TArrayClose : ts) -> pure (ts, els)
        _ -> Nothing


elements :: [Token] -> Maybe ([Token], [JsonValue])
elements tokens =
    let valueAndElementTail = do
            (valueTs, val) <- value tokens
            (elementTailTs, elementTailVals) <- elementTail valueTs
            Just (elementTailTs, val : elementTailVals)
    in
    case tokens of
        (TString _ : _) -> valueAndElementTail
        (TNumber _ : _) -> valueAndElementTail
        (TTrue : _) -> valueAndElementTail
        (TFalse : _) -> valueAndElementTail
        (TNull : _) -> valueAndElementTail
        (TObjectOpen : _) -> valueAndElementTail
        (TArrayOpen : _) -> valueAndElementTail
        _ -> Nothing


elementTail :: [Token] -> Maybe ([Token], [JsonValue])
elementTail tokens =
    case tokens of
        (TComma : ts) -> elements ts
        (TArrayClose : _) -> Just (tokens, [])
        _ -> Nothing