{-# LANGUAGE OverloadedStrings #-}

module Printer where

import Parser (JsonValue (..))
import qualified Data.Text.Lazy as T
import qualified Data.HashMap.Lazy as HM

toText :: JsonValue -> T.Text
toText JsonNull = "null"
toText (JsonBoolean True) = "true"
toText (JsonBoolean False) = "false"
toText (JsonString str) = surround '"' '"' str
toText (JsonNumber num) = T.pack $ show num
toText (JsonArray arr) = surround '[' ']' $ T.intercalate "," $ toText <$> arr
toText (JsonObject obj) = 
    let
        memberToText (key, val) = surround '"' '"' key `T.append` ":" `T.append` toText val
        curlyBraces = surround '{' '}'
    in curlyBraces . T.intercalate "," $ memberToText <$> HM.toList obj

surround :: Char -> Char -> T.Text -> T.Text
surround start end str = T.cons start $ T.snoc str end