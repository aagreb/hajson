{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Lexer as L
import qualified Parser as P
import qualified Printer as PR
import qualified Data.Text.Lazy.IO as TIO

main :: IO ()
main = do
    input  <- TIO.readFile "input.json"
    case L.tokenize input of
        Nothing -> putStrLn "Syntax error!"
        Just tokens -> do
            print tokens
            case P.parse tokens of
                Nothing -> putStrLn "Parse error!"
                Just parsedValue -> do
                    print parsedValue
                    TIO.writeFile "output.json" $ PR.toText parsedValue