let
  pkgs = import (builtins.fetchTarball https://github.com/NixOS/nixpkgs/archive/23.11.tar.gz) {};
in

with pkgs;

mkShell {
  buildInputs = [
    ghc
    cabal-install
    haskell-language-server
  ];
}
